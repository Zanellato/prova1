package inscricao.faces.mngbeans;


import entity.Usuario;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by André on 01/11/2016.
 */

@Named
@RequestScoped
public class LoginBean {

    private Usuario usuario;

    public LoginBean(){
        usuario = new Usuario();
    }

    public String login(){
        if(this.usuario.getNome().equals(this.usuario.getSenha())){
            if(this.usuario.isAdmin()){
                return "admin";
            }
                return "cadastro";
        }
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Acesso negado", "Acesso negado");
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);

        return null;

    }

    public Usuario getUsuario(){
        return usuario;
    }
    public void setUsuario(Usuario user){
        this.usuario = user;
    }

}
